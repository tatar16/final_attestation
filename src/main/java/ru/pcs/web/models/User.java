package ru.pcs.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
//import sun.jvm.hotspot.debugger.cdbg.EnumType;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
//@Entity
public class User {
    public enum Role { ADMIN, USER }
    @Enumerated(value = EnumType.STRING)
    private Role role;// = Role.ADMIN;
//    @GetMapping
    private Integer id;
    private String firstName;
    private String lastName;
 //   @Column(unique = true)
    private String login;
    private String password;
}
