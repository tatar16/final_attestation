package ru.pcs.web.repositories;

import ru.pcs.web.models.Product;

import java.util.List;

public interface ProductsRepository {
    List<Product> findAll();

    void save(Product product);

    void delete(Integer productId);

    void updateProduct(Product product);

    Product findById(Integer productId);
}
