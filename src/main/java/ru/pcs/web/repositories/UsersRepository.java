package ru.pcs.web.repositories;

import ru.pcs.web.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository {
    List<User> findAll();

    void save(User user);

    void delete(Integer userId);

    User findById(Integer userId);

    void updateUser(User user);

    Optional<User> findByLogin(String login);
}
