package ru.pcs.web.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.Product;

import javax.sql.DataSource;
import java.util.List;

@Component
public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {
    //language=SQL
    private static final String SQL_INSERT = "insert into product(name, price, count) values(?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from product order by id";

    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from product where id = ?;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from product where id = ?";

    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update product set name = ?, price = ?, count = ? where id = ?";


    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        Integer id = row.getInt("id");
        String productName = row.getString("name");
        Integer price = row.getInt("price");
        Integer count = row.getInt("count");

        return new Product(id, productName, price, count);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getProductName(), product.getPrice(), product.getCount());
    }

    @Override
    public void delete(Integer productId) {
        jdbcTemplate.update(SQL_DELETE_BY_ID, productId);
    }

    @Override
    public Product findById(Integer productId) {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, productRowMapper, productId);
    }

    @Override
    public void updateProduct(Product product) {
        jdbcTemplate.update(SQL_UPDATE_BY_ID, product.getProductName(), product.getPrice(), product.getCount(),product.getId());

    }
 }


