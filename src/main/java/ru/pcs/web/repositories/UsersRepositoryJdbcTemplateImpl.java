package ru.pcs.web.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.User;

import javax.management.relation.Role;
import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@Component
public class UsersRepositoryJdbcTemplateImpl implements UsersRepository {

  //language=SQL
    private static final String SQL_INSERT = "insert into orderer (first_name, last_name, login, password) values(?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from orderer order by first_name";

    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from orderer where id = ?;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from orderer where id = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_LOGIN = "select * from orderer where login = ?";

    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update orderer set first_name = ?, last_name = ?, login = ?, password = ? where id = ?";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UsersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<User> userRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String firstName = row.getString("first_name");
        String lastName = row.getString("last_name");
        String login = row.getString("login");
        User.Role role = User.Role.valueOf(row.getString("role"));
        String password = row.getString("password");

        return new User(role, id, firstName, lastName, login, password);
    };

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }

    @Override
    public void save(User user) {
        jdbcTemplate.update(SQL_INSERT, user.getFirstName(), user.getLastName(), user.getLogin(), user.getPassword(), user.getRole());
    }

    @Override
    public void delete(Integer userId) {
        jdbcTemplate.update(SQL_DELETE_BY_ID, userId);
    }

    @Override
    public User findById(Integer userId) {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, userRowMapper, userId);
    }

    @Override
    public void updateUser(User user) {
        jdbcTemplate.update(SQL_UPDATE_BY_ID, user.getFirstName(), user.getLastName(), user.getLogin(), user.getPassword(), user.getId());

    }

    @Override
    public Optional<User> findByLogin(String login) {
        return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_BY_LOGIN, userRowMapper, login));

        //        return Optional.empty();
//        Optional<User> user = Optional.of(repository.findById(userId));
    }
}


