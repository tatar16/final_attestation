package ru.pcs.web.forms;

import lombok.Data;

@Data
public class ProductForm {
    private String productName;
    private Integer price;
    private Integer count;
}
