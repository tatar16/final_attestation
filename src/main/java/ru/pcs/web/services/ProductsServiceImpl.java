package ru.pcs.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;
import ru.pcs.web.repositories.ProductsRepository;
import java.util.List;

@Component
public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;

    @Autowired
    public ProductsServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public void addProduct(ProductForm form) {
        Product product = Product.builder()
                .productName(form.getProductName())
                .price(form.getPrice())
                .count(form.getCount())
                .build();
        productsRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public void deleteProduct(Integer productId) { productsRepository.delete(productId);

    }

    @Override
    public Product getProduct(Integer productId) {
        return productsRepository.findById(productId);
    }

    @Override
    public void updateProduct(ProductForm form, Integer productId) {
        Product product = Product.builder()
                .id(productId)
                .productName(form.getProductName())
                .price(form.getPrice())
                .count(form.getCount())
                .build();
        productsRepository.updateProduct(product);

    }
}
