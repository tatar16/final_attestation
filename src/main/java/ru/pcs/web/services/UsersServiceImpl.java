package ru.pcs.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.pcs.web.forms.UserForm;
import ru.pcs.web.models.User;
import ru.pcs.web.repositories.UsersRepository;
import java.util.List;


@Component
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository, PasswordEncoder passwordEncoder) {
        this.usersRepository = usersRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void addUser(UserForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .login(form.getLogin())
                .role(User.Role.ADMIN)
                .password(passwordEncoder.encode(form.getPassword()))
                .build();
        usersRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUser(Integer userId) {
        usersRepository.delete(userId);
    }

    @Override
    public User getUser(Integer userId) {
        return usersRepository.findById(userId);
    }

    @Override
    public void updateUser(UserForm form, Integer userId) {
        User user = usersRepository.findById(userId);
        user.setFirstName(form.getFirstName());
        user.setLastName(form.getLastName());
        user.setLogin(form.getLogin());
        if (form.getPassword().length() > 0) {
            user.setPassword(passwordEncoder.encode(form.getPassword()));
        }
        usersRepository.updateUser(user);
    }
}
